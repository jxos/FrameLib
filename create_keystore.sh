#!/bin/bash
keytool -genkey -alias ksalias -keyalg RSA -keysize 2048 -validity 36500 -keystore keystore.jks
keytool -importkeystore -srckeystore keystore.jks -destkeystore keystore.jks -deststoretype pkcs12
rm -f keystore.jks.old
