#!/bin/bash
keypass=`openssl rand -base64 6`
echo "密钥库口令是：${keypass}"
date=`date +'%Y%m%d'`
jks_name=keystore_${date}_${keypass}.jks
keytool -genkey -alias ksalias -keypass ${keypass} -keyalg RSA -keysize 2048 -validity 36500 -keystore ${jks_name} -storepass ${keypass}
keytool -importkeystore -srckeystore keystore.jks -destkeystore keystore.jks -deststoretype pkcs12
rm -f ${jks_name}.old
