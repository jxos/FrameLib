[![](https://gitee.com/jxos/FrameLib/widgets/widget_3.svg)](https://gitee.com/jxos/FrameLib)

# Android 开源开发框架-AjDevKit（Android Development Kit）

[![](https://www.jitpack.io/v/com.gitee.jxos/FrameLib.svg)](https://www.jitpack.io/#com.gitee.jxos/FrameLib)

#### 介绍

多快好省的Android通用开源开发框架，为项目而生！

#### 如何使用

```
Step 1. Add it in your root build.gradle at the end of repositories:
	allprojects {
		repositories {
            maven { url 'https://www.jitpack.io' }
		}
	}
Step 2. Add the dependency
	dependencies {
        implementation "com.gitee.jxos:FrameLib:Tag"
	}
```

#### 说明

1. 本项目将一直持续维护下去，同时会不断优化改进，请放心使用
2. 支持通用类/函数/方法的定制需求，可评论或私信我，我会根据需求评审

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 赞助

<img src="https://images.gitee.com/uploads/images/2020/0301/103401_c4c1f2fd_69795.png" width = "200" height = "200"/>
<img src="https://images.gitee.com/uploads/images/2020/0301/103856_d02ac844_69795.png" width = "200" height = "200"/>

