备忘：Mac下Android NDK生成.so文件

1，新建TestJni.java，内容如下：
    static {
        System.loadLibrary("TestJni");
    }

    public native static String getString();

2，cd /Users/ZW/Desktop/Jia/WorkSpace/Android/NewApp/app/build/intermediates/classes
3，javah -classpath . -jni com.*.*.bluetooth.TestJni
4，新建com_*_*_bluetooth_TestJni.c文件
5，新建Android.mk文件，内容如下：
LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := TestJni
LOCAL_SRC_FILES := com_*_*_bluetooth_TestJni.c
include $(BUILD_SHARED_LIBRARY)
6，新建Application.mk文件，内容如下：
APP_PLATFORM := android-19
APP_ABI := all
7，关联项目NDK环境
8，配置NDK环境变量
9，cd /Users/ZW/Desktop/Jia/WorkSpace/Android/NewApp/app/src/main/jni
10，ndk-build