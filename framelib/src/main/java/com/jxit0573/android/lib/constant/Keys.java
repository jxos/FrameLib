package com.jxit0573.android.lib.constant;

/**
 * @author jia
 */
public class Keys {

    public static final String ERUDA = "eruda";
    public static final String VCONSOLE = "vConsole";
    public static final String JAVASCRIPT = "javascript";
    public static final String TEL = "tel";
    public static final int TIME_2000 = 2000;

}
