package com.jxit0573.android.lib.util;

import android.os.Bundle;

import com.alibaba.android.arouter.launcher.ARouter;
import com.jxit0573.android.lib.activity.BaseActivity;

/**
 * @author jia
 */
public class RouterUtils {

    private RouterUtils() {
    }

    public static void startActivity(String path, Bundle bundle) {
        ARouter.getInstance().build(path).with(bundle).navigation();
    }

    public static void startActivityAndFinish(BaseActivity activity, String path, Bundle bundle) {
        startActivity(path, bundle);
        activity.finish();
    }

    public static void startActivityForResult(BaseActivity activity, String path, Bundle bundle, int requestCode) {
        ARouter.getInstance().build(path).with(bundle).navigation(activity, requestCode);
    }
}
