package com.jxit0573.android.lib.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.jxit0573.android.lib.common.ViewHolder;
import com.jxit0573.android.lib.util.CommonUtils;

import java.util.List;

/**
 * @author jia
 */
public abstract class AbstractAdapter<T> extends BaseAdapter {

    private final Context context;
    private final List<T> datas;
    private final int itemLayoutId;

    public AbstractAdapter(Context context, List<T> datas, int itemLayoutId) {
        this.context = context;
        this.datas = datas;
        this.itemLayoutId = itemLayoutId;
    }

    @Override
    public int getCount() {
        if (CommonUtils.isEmpty(datas)) {
            return 0;
        } else {
            return datas.size();
        }
    }

    @Override
    public long getItemId(int position) {
        if (CommonUtils.isEmpty(datas)) {
            return -1;
        } else {
            return position;
        }
    }

    @Override
    public T getItem(int position) {
        return datas.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = ViewHolder.getHolder(context, convertView, itemLayoutId);
        getItemView(position, holder, getItem(position));
        return holder.getConvertView();
    }

    /**
     * 获取itemView
     *
     * @param position 当前索引
     * @param holder   holder
     * @param model    对象
     */
    public abstract void getItemView(int position, ViewHolder holder, T model);

}
