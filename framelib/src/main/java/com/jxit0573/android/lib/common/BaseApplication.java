package com.jxit0573.android.lib.common;


import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDexApplication;

import com.alibaba.android.arouter.launcher.ARouter;
import com.jakewharton.threetenabp.AndroidThreeTen;
import com.jxit0573.android.lib.BuildConfig;
import com.jxit0573.android.lib.model.MessageEvent;
import com.jxit0573.android.lib.util.AppUtils;
import com.jxit0573.android.lib.util.FileUtils;
import com.qmuiteam.qmui.arch.QMUISwipeBackActivityManager;
import com.tencent.mmkv.MMKV;
import com.tencent.smtt.export.external.TbsCoreSettings;
import com.tencent.smtt.sdk.QbSdk;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

import xcrash.ICrashCallback;
import xcrash.XCrash;

/**
 * @author jia
 */
public class BaseApplication extends MultiDexApplication {

    private int appCount;

    @Override
    public void onCreate() {
        super.onCreate();
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(@NonNull Activity activity) {
                appCount++;
                if (appCount == 1) {
                    MessageEvent event = new MessageEvent(19103001);
                    EventBus.getDefault().post(event);
                }
            }

            @Override
            public void onActivityResumed(@NonNull Activity activity) {

            }

            @Override
            public void onActivityPaused(@NonNull Activity activity) {

            }

            @Override
            public void onActivityStopped(@NonNull Activity activity) {
                appCount--;
                if (appCount == 0) {
                    MessageEvent event = new MessageEvent(19103002);
                    EventBus.getDefault().post(event);
                }
            }

            @Override
            public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

            }

            @Override
            public void onActivityDestroyed(@NonNull Activity activity) {

            }
        });
        AppUtils.init(this);
        MMKV.initialize(this);
        QMUISwipeBackActivityManager.init(this);
        if (BuildConfig.DEBUG) {
            ARouter.openLog();
            ARouter.openDebug();
        }
        ARouter.init(this);
        Map<String, Object> map = new HashMap<>(16);
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_SPEEDY_CLASSLOADER, true);
        map.put(TbsCoreSettings.TBS_SETTINGS_USE_DEXLOADER_SERVICE, true);
        QbSdk.initTbsSettings(map);
        QbSdk.setDownloadWithoutWifi(true);
        QbSdk.initX5Environment(getApplicationContext(), new QbSdk.PreInitCallback() {
            @Override
            public void onCoreInitFinished() {

            }

            @Override
            public void onViewInitFinished(boolean b) {

            }
        });
        AndroidThreeTen.init(this);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        try {
            ICrashCallback callback = (logPath, emergency) -> {
            };
            PackageInfo packageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            xcrash.XCrash.init(this, new XCrash.InitParameters()
                    .setAppVersion(packageInfo.versionName)
                    .setJavaCallback(callback)
                    .setNativeCallback(callback)
                    .setAnrCallback(callback)
                    .setLogDir(FileUtils.getAppDirFile(base, "/crash").getPath()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
