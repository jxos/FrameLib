package com.jxit0573.android.lib.util;

import android.os.Handler;
import android.os.Looper;

/**
 * @author jia
 */
public class BackgroundTasks {

    private static volatile BackgroundTasks instance;

    private final Handler mHandler = new Handler(Looper.getMainLooper());

    private BackgroundTasks() {
    }

    public static BackgroundTasks getInstance() {
        if (instance == null) {
            synchronized (BackgroundTasks.class) {
                if (instance == null) {
                    instance = new BackgroundTasks();
                }
            }
        }
        return instance;
    }

    public Handler getHandler() {
        return mHandler;
    }

    public boolean post(Runnable r) {
        return mHandler.post(r);
    }

    public boolean postDelayed(Runnable r, long delayMillis) {
        return mHandler.postDelayed(r, delayMillis);
    }
}
