package com.jxit0573.android.lib.util;

/**
 * @author jia
 */
public class RegexUtils {

    private RegexUtils() {
    }

    public static boolean isMobile(String input) {
        return input.matches("^1[3-9][0-9]\\d{8}$");
    }

}
