package com.jxit0573.android.lib.net;

import okhttp3.Call;

/**
 * @author jia
 */
public class RequestCall {

    private Call call;
    private final OkHttpRequest okHttpRequest;

    public RequestCall(OkHttpRequest okHttpRequest) {
        this.okHttpRequest = okHttpRequest;
    }

    private void buildCall() {
        call = RequestUtils.getClient().newCall(generateRequest());
    }

    private okhttp3.Request generateRequest() {
        return okHttpRequest.generateRequest();
    }

    public Call getCall() {
        return call;
    }

    public <T> void execute(BaseRequestCallback<T> callback) {
        buildCall();
        RequestUtils.getInstance().execute(this, callback);
    }
}
