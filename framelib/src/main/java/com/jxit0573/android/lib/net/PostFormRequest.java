package com.jxit0573.android.lib.net;

import java.util.Map;

import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * @author jia
 */
public class PostFormRequest extends OkHttpRequest {

    public PostFormRequest(String url, Object tag, Map<String, String> headers, Map<String, Object> params) {
        super(url, tag, headers, params);
    }

    @Override
    protected RequestBody buildRequestBody() {
        FormBody.Builder formBuilder = new FormBody.Builder();
        if (params != null && params.size() > 0) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                if (entry.getValue() == null) {
                    formBuilder.add(entry.getKey(), "");
                } else {
                    formBuilder.add(entry.getKey(), "" + entry.getValue());
                }
            }
        }
        return formBuilder.build();
    }

    @Override
    protected Request buildRequest(RequestBody requestBody) {
        return builder.post(requestBody).build();
    }
}
