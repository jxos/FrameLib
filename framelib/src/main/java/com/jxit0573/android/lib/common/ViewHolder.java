package com.jxit0573.android.lib.common;

import android.content.Context;
import android.util.SparseArray;
import android.view.View;

/**
 * @author jia
 */
public class ViewHolder {

    private int itemLayoutId;
    private final SparseArray<View> views;
    private final View convertView;

    private ViewHolder(Context context, int itemLayoutId) {
        views = new SparseArray<>();
        convertView = View.inflate(context, itemLayoutId, null);
        convertView.setTag(this);
    }

    public static ViewHolder getHolder(Context context, View convertView, int itemLayoutId) {
        if (convertView == null) {
            return new ViewHolder(context, itemLayoutId);
        } else {
            return (ViewHolder) convertView.getTag();
        }
    }

    public <T extends View> T getView(int id) {
        View childView = views.get(id);
        if (childView == null) {
            childView = convertView.findViewById(id);
            views.put(id, childView);
        }
        return (T) childView;
    }

    public View getConvertView() {
        return convertView;
    }
}