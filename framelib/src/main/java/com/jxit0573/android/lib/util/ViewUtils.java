package com.jxit0573.android.lib.util;

import android.view.View;
import android.widget.EditText;

/**
 * @author jia
 */
public class ViewUtils {

    private ViewUtils() {
    }

    public static void gone(View v) {
        v.setVisibility(View.GONE);
    }

    public static void visible(View v) {
        v.setVisibility(View.VISIBLE);
    }

    public static void invisible(View v) {
        v.setVisibility(View.INVISIBLE);
    }

    public static String getTextTrim(EditText edt) {
        if (edt == null) {
            return null;
        } else {
            return edt.getText().toString().trim();
        }
    }
}
