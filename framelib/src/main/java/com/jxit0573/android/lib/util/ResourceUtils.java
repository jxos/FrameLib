package com.jxit0573.android.lib.util;

import android.content.Context;
import android.util.DisplayMetrics;

import androidx.annotation.ColorRes;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

/**
 * @author jia
 */
public class ResourceUtils {

    private ResourceUtils() {
    }

    public static int getColor(@NonNull Context context, @ColorRes int id) {
        return ContextCompat.getColor(context, id);
    }

    public static String getString(Context context, int id) {
        return context.getResources().getString(id);
    }

    public static float getDimension(Context context, int id) {
        return context.getResources().getDimension(id);
    }

    public static int getDimensionPixelOffset(Context context, int id) {
        return context.getResources().getDimensionPixelOffset(id);
    }

    public static String[] getStringArray(Context context, int id) {
        return context.getResources().getStringArray(id);
    }

    public static float density(Context context) {
        return getDisplayMetrics(context).density;
    }

    public static float scaledDensity(Context context) {
        return getDisplayMetrics(context).scaledDensity;
    }

    public static int heightPixels(Context context) {
        return getDisplayMetrics(context).heightPixels;
    }

    public static int widthPixels(Context context) {
        return getDisplayMetrics(context).widthPixels;
    }

    private static DisplayMetrics getDisplayMetrics(Context context) {
        return context.getResources().getDisplayMetrics();
    }

}