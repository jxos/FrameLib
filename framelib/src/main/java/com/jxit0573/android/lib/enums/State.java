package com.jxit0573.android.lib.enums;

/**
 * @author jia
 */
public enum State {

    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAILTURE

}
