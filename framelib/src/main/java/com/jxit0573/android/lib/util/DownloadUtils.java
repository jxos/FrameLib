package com.jxit0573.android.lib.util;

import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.jxit0573.android.lib.impl.OnDownloadListener;
import com.liulishuo.okdownload.DownloadListener;
import com.liulishuo.okdownload.DownloadTask;
import com.liulishuo.okdownload.OkDownload;
import com.liulishuo.okdownload.core.breakpoint.BreakpointInfo;
import com.liulishuo.okdownload.core.cause.EndCause;
import com.liulishuo.okdownload.core.cause.ResumeFailedCause;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author jia
 */
public class DownloadUtils {

    private DownloadUtils() {
    }

    public static DownloadTask download(String host, String url, File parentFile, String fileName, OnDownloadListener listener) {
        DownloadTask.Builder builder = new DownloadTask.Builder(Objects.requireNonNull(StringUtils.appends(host, url)), parentFile)
                .setMinIntervalMillisCallbackProcess(30);
        if (!TextUtils.isEmpty(fileName)) {
            builder.setFilename(fileName);
        }
        DownloadTask downloadTask = builder.build();
        downloadTask.enqueue(new DownloadListener() {
            @Override
            public void taskStart(@NonNull DownloadTask task) {
            }

            @Override
            public void connectTrialStart(@NonNull DownloadTask task, @NonNull Map<String, List<String>> requestHeaderFields) {
            }

            @Override
            public void connectTrialEnd(@NonNull DownloadTask task, int responseCode, @NonNull Map<String, List<String>> responseHeaderFields) {
            }

            @Override
            public void downloadFromBeginning(@NonNull DownloadTask task, @NonNull BreakpointInfo info, @NonNull ResumeFailedCause cause) {
            }

            @Override
            public void downloadFromBreakpoint(@NonNull DownloadTask task, @NonNull BreakpointInfo info) {
            }

            @Override
            public void connectStart(@NonNull DownloadTask task, int blockIndex, @NonNull Map<String, List<String>> requestHeaderFields) {
            }

            @Override
            public void connectEnd(@NonNull DownloadTask task, int blockIndex, int responseCode, @NonNull Map<String, List<String>> responseHeaderFields) {
            }

            @Override
            public void fetchStart(@NonNull DownloadTask task, int blockIndex, long contentLength) {
                if (listener != null) {
                    listener.onStart(contentLength);
                }
            }

            @Override
            public void fetchProgress(@NonNull DownloadTask task, int blockIndex, long increaseBytes) {
                if (listener != null) {
                    listener.onProgress(increaseBytes);
                }
            }

            @Override
            public void fetchEnd(@NonNull DownloadTask task, int blockIndex, long contentLength) {
            }

            @Override
            public void taskEnd(@NonNull DownloadTask task, @NonNull EndCause cause, @Nullable Exception realCause) {
                OkDownload.with().breakpointStore().remove(task.getId());
                task.cancel();
                if (cause == EndCause.CANCELED) {
                    if (listener != null) {
                        listener.onCancel();
                    }
                } else if (cause == EndCause.ERROR) {
                    if (listener != null) {
                        listener.onError();
                    }
                } else if (cause == EndCause.COMPLETED) {
                    if (listener != null) {
                        listener.onFinish();
                    }
                }
            }
        });
        return downloadTask;
    }
}
