package com.jxit0573.android.lib.net;

/**
 * @author jia
 */
public class PostJsonRequestBuilderBase extends BaseOkHttpRequestBuilder<PostJsonRequestBuilderBase> {

    @Override
    public RequestCall build() {
        return new PostJsonRequest(url, tag, headers, params).build();
    }
}
