package com.jxit0573.android.lib.util;

import android.os.Parcelable;

import com.tencent.mmkv.MMKV;

import java.util.Set;

/**
 * @author jia
 */
public class MmkvUtils {

    private static volatile MmkvUtils instance;

    private static final MMKV MK;

    static {
        MK = MMKV.defaultMMKV();
    }

    private MmkvUtils() {
    }

    public static MmkvUtils getInstance() {
        if (instance == null) {
            synchronized (MmkvUtils.class) {
                if (instance == null) {
                    instance = new MmkvUtils();
                }
            }
        }
        return instance;
    }

    public MmkvUtils put(String key, boolean value) {
        MK.encode(key, value);
        return instance;
    }

    public boolean getFalse(String key) {
        return MK.decodeBool(key, false);
    }

    public boolean getTrue(String key) {
        return MK.decodeBool(key, true);
    }

    public MmkvUtils put(String key, int value) {
        MK.encode(key, value);
        return instance;
    }

    public int getInt(String key) {
        return MK.decodeInt(key);
    }

    public int getInt(String key, int defaultValue) {
        return MK.decodeInt(key, defaultValue);
    }

    public MmkvUtils put(String key, long value) {
        MK.encode(key, value);
        return instance;
    }

    public long getLong(String key) {
        return MK.decodeLong(key);
    }

    public long getLong(String key, long defaultValue) {
        return MK.decodeLong(key, defaultValue);
    }

    public MmkvUtils put(String key, float value) {
        MK.encode(key, value);
        return instance;
    }

    public float getFloat(String key) {
        return MK.decodeFloat(key);
    }

    public float getFloat(String key, float defaultValue) {
        return MK.decodeFloat(key, defaultValue);
    }

    public MmkvUtils put(String key, double value) {
        MK.encode(key, value);
        return instance;
    }

    public double getDouble(String key) {
        return MK.decodeDouble(key);
    }

    public double getDouble(String key, double defaultValue) {
        return MK.decodeDouble(key, defaultValue);
    }

    public MmkvUtils put(String key, String value) {
        MK.encode(key, value);
        return instance;
    }

    public String getString(String key) {
        return MK.decodeString(key);
    }

    public String getString(String key, String defaultValue) {
        return MK.decodeString(key, defaultValue);
    }

    public MmkvUtils put(String key, Set<String> value) {
        MK.encode(key, value);
        return instance;
    }

    public Set<String> getStringSet(String key) {
        return MK.decodeStringSet(key);
    }

    public Set<String> getStringSet(String key, Set<String> defaultValue) {
        return MK.decodeStringSet(key, defaultValue);
    }

    public MmkvUtils put(String key, byte[] value) {
        MK.encode(key, value);
        return instance;
    }

    public byte[] getBytes(String key) {
        return MK.decodeBytes(key);
    }

    public byte[] getBytes(String key, byte[] defaultValue) {
        return MK.decodeBytes(key, defaultValue);
    }

    public MmkvUtils put(String key, Parcelable value) {
        MK.encode(key, value);
        return instance;
    }

    public <T extends Parcelable> T getParcelable(String key, Class<T> tClass) {
        return MK.decodeParcelable(key, tClass);
    }

    public <T extends Parcelable> T getParcelable(String key, Class<T> tClass, T defaultValue) {
        return MK.decodeParcelable(key, tClass, defaultValue);
    }

    public boolean contains(String key) {
        return MK.containsKey(key);
    }

    public MmkvUtils remove(String key) {
        MK.removeValueForKey(key);
        return instance;
    }

    public void removeKeys(String[] keys) {
        MK.removeValuesForKeys(keys);
    }

    public void clear() {
        MK.clearAll();
    }

}
