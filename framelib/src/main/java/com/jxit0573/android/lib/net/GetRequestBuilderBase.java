package com.jxit0573.android.lib.net;

import com.jxit0573.android.lib.util.CommonUtils;
import com.jxit0573.android.lib.util.StringUtils;

import java.util.Map;

/**
 * @author jia
 */
public class GetRequestBuilderBase extends BaseOkHttpRequestBuilder<GetRequestBuilderBase> {

    @Override
    public RequestCall build() {
        if (params != null && params.size() > 0) {
            url = appendParams(url, params);
        }
        return new GetRequest(url, tag, headers, params).build();
    }

    private String appendParams(String url, Map<String, Object> params) {
        if (CommonUtils.isEmpty(params)) {
            return url;
        } else {
            return StringUtils.appends(url, "?", StringUtils.join(params));
        }
    }
}
