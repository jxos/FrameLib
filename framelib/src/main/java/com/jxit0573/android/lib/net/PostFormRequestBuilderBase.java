package com.jxit0573.android.lib.net;

/**
 * @author jia
 */
public class PostFormRequestBuilderBase extends BaseOkHttpRequestBuilder<PostFormRequestBuilderBase> {

    @Override
    public RequestCall build() {
        return new PostFormRequest(url, tag, headers, params).build();
    }
}
