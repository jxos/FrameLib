package com.jxit0573.android.lib.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jxit0573.android.lib.R;
import com.jxit0573.android.lib.widget.X5WebView;
import com.tencent.smtt.sdk.ValueCallback;

/**
 * @author jia
 */
@Route(path = "/frame/H5Activity")
public class H5Activity extends BaseActivity implements X5WebView.OnWebViewCallback {

    private RelativeLayout rlH5;

    private X5WebView wvH5;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_h5);

        initView();
        initData();
    }

    @Override
    public void initView() {
        rlH5 = findViewById(R.id.rl_h5);
        wvH5 = new X5WebView(this);
        wvH5.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
        rlH5.addView(wvH5);
        wvH5.setUserAgent("jia_frame");
        wvH5.setCallback(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    public void initData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            finish();
            return;
        }
        String url = bundle.getString("url").trim();
        if (TextUtils.isEmpty(url)) {
            finish();
            return;
        }
        wvH5.loadUrl(url);
    }

    @Override
    public void getData() {

    }

    @Override
    public void setData() {

    }

    @Override
    public void onUrlCallback(String url) {

    }

    @Override
    public void openFileChooser(ValueCallback<Uri> uploadMsg) {

    }

    @Override
    public void onShowFileChooser(ValueCallback<Uri[]> uploadMsg) {

    }

    @Override
    public void onReceivedTitle(String title) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK)
                && (event.getAction() == KeyEvent.ACTION_DOWN)
                && (event.getRepeatCount() == 0)
                && wvH5.canGoBack()) {
            wvH5.goBack();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onDestroy() {
        if (wvH5 != null) {
            ViewParent parent = wvH5.getParent();
            if (parent != null) {
                ((ViewGroup) parent).removeView(wvH5);
            }
            rlH5.removeAllViews();
            rlH5 = null;
            wvH5.loadDataWithBaseURL(null, "", "text/html", "utf-8", null);
            wvH5.stopLoading();
            wvH5.clearHistory();
            wvH5.setWebViewClient(null);
            wvH5.setWebChromeClient(null);
            wvH5.removeAllViews();
            wvH5.destroy();
            wvH5 = null;
        }
        super.onDestroy();
    }
}
