package com.jxit0573.android.lib.impl;

/**
 * @author jia
 */
public interface OnDownloadListener {

    /**
     * 下载开始
     *
     * @param contentLength 总长度
     */
    void onStart(long contentLength);

    /**
     * 下载进行中
     *
     * @param increaseBytes 下载增量
     */
    void onProgress(long increaseBytes);

    /**
     * 下载取消
     */
    void onCancel();

    /**
     * 下载失败
     */
    void onError();

    /**
     * 下载结束
     */
    void onFinish();
}
