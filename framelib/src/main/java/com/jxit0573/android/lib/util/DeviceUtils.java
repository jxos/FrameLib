package com.jxit0573.android.lib.util;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import androidx.core.content.ContextCompat;

/**
 * @author jia
 */
public class DeviceUtils {

    private DeviceUtils() {
    }

    /**
     * 获取主板名称
     */
    public static String getBoard() {
        return Build.BOARD.toLowerCase().trim();
    }

    /**
     * 获取设备制造商
     */
    public static String getManufacturer() {
        return Build.MANUFACTURER.toLowerCase().trim();
    }

    /**
     * 获取设备品牌
     */
    public static String getBrand() {
        return Build.BRAND.toLowerCase().trim();
    }

    /**
     * 获取手机型号
     */
    public static String getModel() {
        return Build.MODEL.toLowerCase().trim();
    }

    /**
     * 获取系统版本
     */
    public static String getRelease() {
        return Build.VERSION.RELEASE.toLowerCase().trim();
    }

    /**
     * 获取系统API级别
     */
    public static int getSdk() {
        return Build.VERSION.SDK_INT;
    }

}
