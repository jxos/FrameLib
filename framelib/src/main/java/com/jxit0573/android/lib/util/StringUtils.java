package com.jxit0573.android.lib.util;

import android.text.TextUtils;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

import org.threeten.bp.Instant;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.ZoneOffset;
import org.threeten.bp.format.DateTimeFormatter;

import java.security.MessageDigest;
import java.util.List;
import java.util.Map;

/**
 * @author jia
 */
public class StringUtils {

    private static final char[] DIGITS_UPPER = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    private static final char[] DIGITS_LOWER = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    private StringUtils() {
    }

    public static String getMessageDigest(byte[] bytes) {
        try {
            MessageDigest messageDigest;
            (messageDigest = MessageDigest.getInstance("MD5")).update(bytes);
            int var8;
            char[] var3 = new char[(var8 = (bytes = messageDigest.digest()).length) * 2];
            int var4 = 0;
            for (int i = 0; i < var8; ++i) {
                byte var6 = bytes[i];
                var3[var4++] = DIGITS_LOWER[var6 >>> 4 & 15];
                var3[var4++] = DIGITS_LOWER[var6 & 15];
            }
            return new String(var3);
        } catch (Exception var8) {
            return null;
        }
    }

    /**
     * 左补齐
     */
    public static String leftPad(String input, int length, int val) {
        return String.format("%0" + (length - input.length()) + "d", val) + input;
    }

    /**
     * 转Java系统时间：毫秒
     */
    public static long toJavaMsec(long unixTimestamp) {
        return unixTimestamp * 1000L;
    }

    /**
     * 转Unix timestamp：秒
     */
    public static long toUnixTimestamp(long javaMsec) {
        return javaMsec / 1000L;
    }

    /**
     * 获取时间戳，毫秒
     */
    public static long getMillisecond() {
        return Instant.now().toEpochMilli();
    }

    /**
     * 时间格式化
     * yyyy-MM-dd HH:mm:ss
     */
    public static String formatToSecond(long epochMilli) {
        LocalDateTime dateTime;
        if (epochMilli == 0) {
            dateTime = LocalDateTime.now();
        } else {
            dateTime = Instant.ofEpochMilli(epochMilli).atZone(ZoneOffset.ofHours(8)).toLocalDateTime();
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        return dateTime.format(dtf);
    }

    /**
     * 时间格式化
     * yyyy-MM-dd HH:mm
     */
    public static String formatToMinute(long epochMilli) {
        LocalDateTime dateTime;
        if (epochMilli == 0) {
            dateTime = LocalDateTime.now();
        } else {
            dateTime = Instant.ofEpochMilli(epochMilli).atZone(ZoneOffset.ofHours(8)).toLocalDateTime();
        }
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        return dateTime.format(dtf);
    }

    /**
     * 时间格式化
     * yyyy-MM-dd
     */
    public static String formatToDay(long epochMilli) {
        if (epochMilli == 0) {
            return LocalDateTime.now().toString();
        } else {
            return Instant.ofEpochMilli(epochMilli).atZone(ZoneOffset.ofHours(8)).toLocalDateTime().toString();
        }
    }

    public static String appends(Object... objs) {
        if (CommonUtils.isEmpty(objs)) {
            return null;
        } else {
            StringBuilder sb = new StringBuilder();
            for (Object o : objs) {
                sb.append(o);
            }
            return sb.toString();
        }
    }

    public static String bytes2HexString(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        int len = bytes.length;
        char[] out = new char[len << 1];
        for (int i = 0, j = 0; i < len; i++) {
            out[j++] = DIGITS_UPPER[(0xF0 & bytes[i]) >>> 4];
            out[j++] = DIGITS_UPPER[0x0F & bytes[i]];
        }
        return new String(out);
    }

    public static byte[] hexString2Bytes(String hexString) {
        if (TextUtils.isEmpty(hexString)) {
            return null;
        }
        hexString = hexString.trim().toUpperCase();
        int length = hexString.length() / 2;
        char[] hexChars = hexString.toCharArray();
        byte[] d = new byte[length];
        for (int i = 0; i < length; i++) {
            int pos = i * 2;
            d[i] = (byte) (charToByte(hexChars[pos]) << 4 | charToByte(hexChars[pos + 1]));
        }
        return d;
    }

    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);
    }

    public static String join(List<String> list, String separator) {
        if (CommonUtils.isEmpty(list)) {
            return null;
        } else {
            return Joiner.on(separator).join(list);
        }
    }

    public static String join(Map<String, Object> map) {
        if (CommonUtils.isEmpty(map)) {
            return null;
        } else {
            return Joiner.on("&").withKeyValueSeparator("=").join(map);
        }
    }

    public static Map<String, String> split(String params) {
        if (TextUtils.isEmpty(params)) {
            return null;
        } else {
            return Splitter.on("&").withKeyValueSeparator("=").split(params);
        }
    }

    public static List<String> split(String params, String separator) {
        if (TextUtils.isEmpty(params)) {
            return null;
        } else {
            return Splitter.on(separator).splitToList(params);
        }
    }
}
