package com.jxit0573.android.lib.impl;

/**
 * @author jia
 */
public interface OnActionListener {

    /**
     * 确定
     *
     * @param str 内容
     */
    void onConfirm(String str);

    /**
     * 取消
     */
    void onCancel();
}
