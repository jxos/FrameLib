package com.jxit0573.android.lib.net;

/**
 * @author jia
 */
public class UploadRequestBuilderBase extends BaseOkHttpRequestBuilder<UploadRequestBuilderBase> {

    @Override
    public RequestCall build() {
        return new UploadRequest(url, tag, headers, params).build();
    }
}
