package com.jxit0573.android.lib.net;

import com.jxit0573.android.lib.util.JsonUtils;

import java.util.Map;

import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * @author jia
 */
public class PostJsonRequest extends OkHttpRequest {

    public PostJsonRequest(String url, Object tag, Map<String, String> headers, Map<String, Object> params) {
        super(url, tag, headers, params);
    }

    @Override
    protected RequestBody buildRequestBody() {
        return RequestBody.create(MediaType.parse("application/json; charset=UTF-8"), JsonUtils.toJson(params));
    }

    @Override
    protected Request buildRequest(RequestBody requestBody) {
        return builder.post(requestBody).build();
    }
}
