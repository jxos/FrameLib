package com.jxit0573.android.lib.model;

import java.io.Serializable;

/**
 * @author jia
 */
public class MessageEvent implements Serializable {

    private static final long serialVersionUID = 1L;

    private int what;
    private Object[] objs;

    public MessageEvent(int what, Object... objs) {
        this.what = what;
        this.objs = objs;
    }

    public int getWhat() {
        return what;
    }

    public void setWhat(int what) {
        this.what = what;
    }

    public Object[] getObjs() {
        return objs;
    }

    public void setObjs(Object[] objs) {
        this.objs = objs;
    }
}
