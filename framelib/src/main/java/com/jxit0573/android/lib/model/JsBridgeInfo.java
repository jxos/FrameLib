package com.jxit0573.android.lib.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.alibaba.fastjson.JSONObject;

/**
 * @author jia
 */
public class JsBridgeInfo implements Parcelable {

    public static final Parcelable.Creator<JsBridgeInfo> CREATOR = new Parcelable.Creator<JsBridgeInfo>() {
        @Override
        public JsBridgeInfo createFromParcel(Parcel source) {
            return new JsBridgeInfo(source);
        }

        @Override
        public JsBridgeInfo[] newArray(int size) {
            return new JsBridgeInfo[size];
        }
    };
    private long msgid;
    private String method;
    private JSONObject params;

    public JsBridgeInfo() {
    }

    protected JsBridgeInfo(Parcel in) {
        this.msgid = in.readLong();
        this.method = in.readString();
        this.params = (JSONObject) in.readSerializable();
    }

    public long getMsgid() {
        return msgid;
    }

    public void setMsgid(long msgid) {
        this.msgid = msgid;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public JSONObject getParams() {
        return params;
    }

    public void setParams(JSONObject params) {
        this.params = params;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.msgid);
        dest.writeString(this.method);
        dest.writeSerializable(this.params);
    }
}
