package com.jxit0573.android.lib.net;

import java.util.Map;

import okhttp3.RequestBody;

/**
 * @author jia
 */
public class GetRequest extends OkHttpRequest {

    public GetRequest(String url, Object tag, Map<String, String> headers, Map<String, Object> params) {
        super(url, tag, headers, params);
    }

    @Override
    protected RequestBody buildRequestBody() {
        return null;
    }

    @Override
    protected okhttp3.Request buildRequest(RequestBody requestBody) {
        return builder.get().build();
    }
}
