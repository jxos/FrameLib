package com.jxit0573.android.lib.net;

import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author jia
 */
public abstract class BaseOkHttpRequestBuilder<T extends BaseOkHttpRequestBuilder> {

    protected String url;
    protected Map<String, String> headers;
    protected Map<String, Object> params;
    protected Object tag;

    public T url(String url) {
        this.url = url;
        return (T) this;
    }

    public T addHeader(String key, String value) {
        if (headers == null) {
            headers = Maps.newLinkedHashMapWithExpectedSize(7);
        }
        headers.put(key, value);
        return (T) this;
    }

    public T headers(Map<String, String> headers) {
        this.headers = headers;
        return (T) this;
    }

    public T params(Map<String, Object> params) {
        this.params = params;
        return (T) this;
    }

    public T tag(Object tag) {
        this.tag = tag;
        return (T) this;
    }

    /**
     * 构建RequestCall对象
     *
     * @return RequestCall对象
     */
    public abstract RequestCall build();
}
