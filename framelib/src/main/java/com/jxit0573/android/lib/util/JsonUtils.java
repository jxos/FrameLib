package com.jxit0573.android.lib.util;

import android.text.TextUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

import java.lang.reflect.Type;
import java.util.List;

/**
 * @author jia
 */
public class JsonUtils {

    private JsonUtils() {
    }

    public static <T> T parse(String json, Type type) {
        if (TextUtils.isEmpty(json)) {
            return null;
        } else {
            return JSON.parseObject(json, type);
        }
    }

    public static <T> T getBean(String json, Class<T> clazz) {
        if (TextUtils.isEmpty(json)) {
            return null;
        } else {
            return JSON.parseObject(json, clazz);
        }
    }

    public static <T> List<T> getList(String json, Class<T> clazz) {
        if (TextUtils.isEmpty(json)) {
            return null;
        } else {
            return JSON.parseArray(json, clazz);
        }
    }

    public static String toJson(Object object) {
        return JSON.toJSONString(object, SerializerFeature.WriteMapNullValue);
    }
}
