package com.jxit0573.android.demo.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import androidx.annotation.Nullable;

import com.google.common.collect.Maps;
import com.jxit0573.android.demo.R;
import com.jxit0573.android.lib.activity.BaseActivity;
import com.jxit0573.android.lib.constant.Keys;
import com.jxit0573.android.lib.util.StringUtils;
import com.tbruyelle.rxpermissions3.RxPermissions;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.rxjava3.disposables.Disposable;

/**
 * @author jia
 */
public class SplashActivity extends BaseActivity {

    private static final String REG = "#.*\\?";

    long current;
    private Disposable disposable;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        RxPermissions rxPermissions = new RxPermissions(this);
        disposable = rxPermissions.request(
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.CAMERA)
                .subscribe(granted -> {
                    if (granted) {
                    } else {
                    }
                });

        String url = "http://host:8081/static/apps/wms/index.html?key0=-1&key1=value1&key2=中文#/a/b";

        Pattern pattern = Pattern.compile(REG);
        Matcher matcher = pattern.matcher(url);
        String group = "";
        if (matcher.find()) {
            group = matcher.group();
            url = matcher.replaceAll("?");
        }

        Uri uri = Uri.parse(url);

        String devTool = uri.getQueryParameter("devTool");
        if (!TextUtils.isEmpty(devTool)) {
            showDevTool(null, StringUtils.appends("javascript:", devTool));
        }

        Uri.Builder builder = uri.buildUpon();
        builder.appendQueryParameter("key3", "%E7%BC%96%E7%A0%81")
                .appendQueryParameter("key4", "true");
        url = builder.build().toString();

        if (!TextUtils.isEmpty(group)) {
            url = url.replace("?", group);
        }

        Log.e("URL", url);
    }

    @Override
    protected void onDestroy() {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
        super.onDestroy();
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void getData() {

    }

    @Override
    public void setData() {

    }

    @Override
    public void onViewClick(View v) {
        super.onViewClick(v);
        if (v.getId() == R.id.btn_1) {
            Map<String, Object> params = Maps.newHashMapWithExpectedSize(7);
//            showDevTool(null,"javascript:eruda");
//            showDevTool(null,"javascript:vConsole");
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            String value = data.getStringExtra("value");
            if (!TextUtils.isEmpty(value)) {
                if (value.startsWith(Keys.JAVASCRIPT)) {
                    showDevTool(null, value);
                }
            }
        }
    }

}
