package com.jxit0573.android.demo.core;

import com.didichuxing.doraemonkit.DoKit;
import com.jxit0573.android.lib.common.BaseApplication;

/**
 * @author jia
 */
public class DemoApplication extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        new DoKit.Builder(this).build();
    }
}
