package com.jxit0573.android.demo.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.alibaba.android.arouter.facade.annotation.Route;
import com.jxit0573.android.demo.R;
import com.jxit0573.android.demo.constant.Router;
import com.jxit0573.android.lib.activity.BaseActivity;

/**
 * @author jia
 */
@Route(path = Router.TEST_ACTIVITY)
public class TestActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
    }

    @Override
    public void initView() {

    }

    @Override
    public void initData() {

    }

    @Override
    public void getData() {

    }

    @Override
    public void setData() {

    }
}
